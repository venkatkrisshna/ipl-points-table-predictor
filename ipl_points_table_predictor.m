% ====================== %
% POINTS TABLE PREDICTOR %
% ====================== %
% Author: Venkata Krisshna
% -------------------------------------------------------
% This program predicts all possible outcomes in a league
% -------------------------------------------------------
clear; clc;
global n tms otms opts onrr nout


% Current league status inputs
% ----------------------------
n = 8; % number of teams
tms = ["MI" "RCB" "DC" "SRH" "KXIP" "RR" "KKR" "CSK"]; % team names
pts = [16    14   14    10    12    10   12    10 ]; % team points
nrr = [1.107 -.172 -.109 .608 -.162 -.569 -.214 -.569]; % team NRRs
rm  = ["KXIP"  "RR"
       "DC"  "MI"
       "RCB"  "SRH"
       "CSK"  "KXIP"
       "KKR"  "RR"
       "DC"   "RCB"
       "SRH"  "MI"]; % remaining matches
wpts = 2; % points for winner
dpts = 1; % points for draw
lpts = 0; % points for loser
mytn = "KXIP"; % your team name
mytp = "playoffs"; % desired position of your team at the end of the league
% ["playoffs" "1" "2" "3" "4" "1-2" "3-4"]


% Verifying the initial state of the of table
% -------------------------------------------
% Number of teams and number of datapoints
if (n~=length(tms) || n~=length(pts) || n~=length(nrr))
    fprintf("Conflict in number of teams!\n<! terminating simulator !>\n")
    return
end
% Your team identification
pp=0;
for nn=1:n
    if (mytn==tms(nn))
        pp=1; 
    end
end
if (pp==0)
    fprintf("Your team is unidentified!\n<! terminating simulator !>\n")
    return
end
% Number of remaining matches
nrm = size(rm,1); 
if (nrm<1)
    fprintf("No more matches to be played!\n<! terminating simulator !>\n")
    return
end
% Remaining matches team unidentified
for i=1:nrm
    for j=1:2
        pp=0;
        for nn=1:n
            if (rm(i,j)==tms(nn))
                pp=1;
            end
        end
        if (pp==0)
            fprintf("Unidentified team in remaining matches!\n<! terminating simulator !>\n")
            return
        end
    end
end
% Team name conflict
for i=1:n
    for j=1:n
        if (i~=j && tms(i)==tms(j))
            fprintf("Conflict in team names!\n<! terminating simulator !>\n") % Add position
            return
        end
    end
end
% Desired outcome unidentified
if (not(contains(mytp,["play" "1" "2" "3" "4" "1-2" "3-4"])))
    fprintf("Unidentified desired outcome!\n<! terminating simulator !>\n")
    return
end


% Computing outcomes of events
% ----------------------------
% Outcomes of individual event
drw = "DRW"; % draw state
ievt = strings(nrm,3);
for i=1:nrm
    ievt(i,:) = [rm(i,1),rm(i,2),drw]; % individual event outcomes (win/lose/draw)
end
% Outcomes of all events
nout = 3^nrm;             % number of outcomes
cevt = strings(nout,nrm); % possible outcomes
for j=1:nrm
    jj=1;
    f = 3^(nrm-j); % period of individual outcome
    for i=1:nout
        cevt(i,j) = ievt(j,jj);
        if (mod(i,f)==0) % checking if period is complete
            if (jj<3)
                jj=jj+1; % moving to next individual outcome
            else
                jj=1; % reseting to first individaul outcome
            end
        end
    end
end


% Computing new states
% --------------------
opts = zeros(nout,n);   % outcomes of points
otms = strings(nout,n); % outcomes of standings
onrr = zeros(nout,n);   % outcomes of NRRs
for i=1:nout
    opts(i,:) = pts; % set to old
    otms(i,:) = tms; % set to old
    onrr(i,:) = nrr; % set to old
end
% Scrolling through outcomes
for i=1:nout
    for j=1:nrm
        if(cevt(i,j)==drw)
            % Draw game
            for ii=1:2
                for nn=1:n
                    if (rm(j,ii)==tms(nn)) % find both teams
                        opts(i,nn) = opts(i,nn)+dpts; % add draw points
                    end
                end
            end
        else
            % Win/lose game
            for nn=1:n
                if (cevt(i,j)==tms(nn)) % find team
                    opts(i,nn) = opts(i,nn)+wpts; % add win points
                    break
                end
            end
        end
    end
end
arrange("0") % arranging with -inf NRR for my team
sotms=otms; sopts=opts; sonrr=onrr; % recording standard outcomes


% Look for qualified teams
% ------------------------
qual = strings(1,4); % list of qualified teams
qcount=0;
for j=1:4 % scroll through each team in top 4 for (say, first) outcome
    p=1; % qualification checker
    for i=2:nout % scroll through outcomes
        myn=0;
        for nn=1:4
            if (otms(1,j)==otms(i,nn) && opts(i,nn)>opts(i,5)) % find team in top 4, check for NRR game
                myn=nn;
                break
            end
        end
        if (myn==0) % if team doesn't show up in top 4 of this outcome
            p=0;  % cannot qualify
            break
        end
    end
    if (p==1)
        qcount=qcount+1;
        qual(qcount)=otms(1,j); % qualified
    end
end
for i=1:4
    if (qual(i)~="")
        fprintf(qual(i)+' have qualified for the playoffs!\n')
    end
end


% Likelihood of desired outcome for your team
% -------------------------------------------
likelihood=zeros(1,n);
unlikelihood=zeros(1,n);
netrunrate=zeros(1,n);
route=strings(nout,nrm);
unroute=strings(nout,nrm);
nrrroute=strings(nout,nrm);
nrrloc=zeros(1,nout);
for nt=1:n
    like=0;
    nrrg=0;
    unlike=0;
    otms=sotms; opts=sopts; onrr=sonrr; % resetting to standard outcomes
    arrange(tms(nt)) % arrange for -inf NRR for current team
    for i=1:nout % scroll through outcomes
        if (mytp=="playoffs")
            outcase = mytp;
            if (contains(tms(nt),otms(i,1:4)))
                like=like+1;
                if (tms(nt)==mytn); route(like,:)=cevt(i,:); end
            else
                unlike=unlike+1;
                if (tms(nt)==mytn); unroute(unlike,:)=cevt(i,:); end
            end
        elseif (mytp=="1-2")
            if (contains(tms(nt),otms(i,1:2)))
                like=like+1;
                if (tms(nt)==mytn); route(like,:)=cevt(i,:); end
            else
                unlike=unlike+1;
                if (tms(nt)==mytn); unroute(unlike,:)=cevt(i,:); end
            end
            outcase = "top 2";
        elseif (mytp=="3-4")
            if (contains(tms(nt),otms(i,3:4)))
                like=like+1;
                if (tms(nt)==mytn); route(like,:)=cevt(i,:); end
            else
                unlike=unlike+1;
                if (tms(nt)==mytn); unroute(unlike,:)=cevt(i,:); end
            end
            outcase = "positions 3-4";
        else
            mytpp = str2double(mytp);
            if (contains(tms(nt),otms(i,mytpp)))
                like=like+1;
                if (tms(nt)==mytn); route(like,:)=cevt(i,:); end
            else
                unlike=unlike+1;
                if (tms(nt)==mytn); unroute(unlike,:)=cevt(i,:); end
            end
            outcase = strcat("position ",mytp);
        end
        % NRR disadvantage
        for nn=1:n
            if (otms(i,nn)==tms(nt))
                if (nn>4 && opts(i,nn)==opts(i,4))
                    nrrg=nrrg+1;
                    
                    if (tms(nt)==mytn)
                        nrrroute(nrrg,:)=cevt(i,:);
                        nrrloc(nrrg)=i;
                    end
                end
                break
            end
        end
    end
    likelihood(nt) = 100*like/nout;
    netrunrate(nt) = 100*nrrg/nout;
    unlikelihood(nt) = 100*unlike/nout;
end
for nt=1:n
    if (mytn==tms(nt))
        fprintf('-------------------------------------------------------------\n');
        fprintf(mytn+' have a %0.1f%% likelihood of making it to '+outcase+'!\n',likelihood(nt));
        fprintf(mytn+' have a %0.1f%% chance of missing playoffs due to a NRR disadvantage!\n',netrunrate(nt));
        fprintf(mytn+' have a %0.1f%% chance of disqualifying!\n',100-likelihood(nt));
        fprintf('-------------------------------------------------------------\n');
    end
end
for nt=1:n
    fprintf(tms(nt)+': %0.1f%% '+outcase+'| %0.1f%% missing due to NRR | %0.1f%% disqualifying!\n',likelihood(nt),netrunrate(nt),100-likelihood(nt));
end
otms=sotms; opts=sopts; onrr=sonrr; % resetting to standard outcomes

% Backtracing
% -----------
cutoff=30;
for nt=1:n
    if (tms(nt)==mytn)
        fprintf('----------------------------------------------------------------\n');
        fprintf(mytn+' have a %0.1f%% likelihood of making it to '+outcase+'!\n',likelihood(nt));
        otms=sotms; opts=sopts; onrr=sonrr; % resetting to standard outcomes
        arrange(tms(nt)) % arrange for -inf NRR for current team
        if (likelihood(nt)>0 && likelihood(nt)<100)
            % Match by match necessary outcomes
            pr=0;
            for j=1:nrm
                p=0;
                d=1;
                for i=1:2
                    if (not(contains(rm(j,i),route(1:round(likelihood(nt)*nout/100),j)))) % if either team doesn't show up in route array for this match
                        p=i;
                    end
                end
                if (not(contains(drw,route(1:round(likelihood(nt)*nout/100),j)))) % if no draw games for this match
                    d=0;
                end
                if (p==1)
                    p=2;
                elseif (p==2)
                    p=1;
                end
                if (p~=0)
                    if (pr==0)
                        fprintf('------------------------------\n');
                        fprintf('MATCH BY MATCH STATS\n');
                        fprintf('------------------------------\n');
                        pr=1;
                    end
                    if (d~=0)
                        fprintf('Match %i: '+rm(j,p)+' has to win/draw!\n',j); % then that that team has to lose this match
                    elseif (d==0)
                        fprintf('Match %i: '+rm(j,p)+' has to win!\n',j); % then that that team has to lose this match
                    end
                end
            end
            % NRR games
            otms=sotms; opts=sopts; onrr=sonrr; % resetting to standard outcomes
            for i=1:round(netrunrate(nt)*nout/100)
                for j=1:nrm
                    for nn=1:n
                        if (nrrroute(i,j)==otms(nrrloc(i),nn)) % find team position in this outcome
                            otn=nn;
                            break
                        end
                    end
                    for nn=1:n
                        if (mytn==otms(nrrloc(i),nn)) % find team position in this outcome
                            myn=nn;
                            break
                        end
                    end
                    if (otms(nrrloc(i),otn)~=mytn && opts(nrrloc(i),otn)==opts(nrrloc(i),4))
                        if (onrr(nrrloc(i),nn)<onrr(nrrloc(i),myn))
                            fprintf(otms(nrrloc(i),otn)+' has to win marginally!\n')
                        end
                    end
                end
            end
        end
        if (likelihood(nt)>0 && likelihood(nt)<=cutoff)
            % Possible routes to desired outcome
            fprintf('----------------------------------------------------------------\n');
            fprintf('Here are the possible individual game results that can lead to the DESIRABLE state for '+mytn+':\n');
            for i=1:round(likelihood(nt)*nout/100)
                for j=1:nrm
                    fprintf('%i: Match %i: '+rm(j,1)+' vs '+rm(j,2)+': '+route(i,j)+' | ',i,j);
                end
                fprintf('\n')
            end
        elseif (likelihood(nt)<100 && likelihood(nt)>100-cutoff)
            fprintf('Here are the possible individual game results that can lead to an UNDESIRABLE state for '+mytn+':\n');
            for i=1:round(unlikelihood(nt)*nout/100)
                for j=1:nrm
                    fprintf('%i: Match %i: '+rm(j,1)+' vs '+rm(j,2)+': '+unroute(i,j)+' | ',i,j);
                end
                fprintf('\n')
            end
        end
        break
    end
end

fprintf('----------------------------------------------------------------\n');

% Function to arrange table in descending order of points and NRR
function arrange(mytn)
    global n otms opts onrr nout
    % Rearrange table in descending order of points
    conv=0;
    while(conv==0)
        conv=1;
        for i=1:nout
            if (mytn~="0")
                % Set NRR of your team to -inf
                for j=1:n
                    if (otms(i,j)==mytn)
                        break
                    end
                end
                onrr(i,j)=-Inf;
            end
            for j=1:n-1
                 % Comparing points and NRR for equal points
                if (opts(i,j)<opts(i,j+1) || (opts(i,j)==opts(i,j+1) && onrr(i,j)<onrr(i,j+1)))
                    swap(i,j,j+1);
                    conv=0;
                end
            end
            % Checking last point and NRR
            if (opts(i,n)>opts(i,n-1) || (opts(i,n)==opts(i,n-1) && onrr(i,n)>onrr(i,n-1)))
                swap(i,n,n-1);
                conv=0;
            end
        end
    end
end

% Function to swap positions of two teams on the table
function swap(i,j1,j2)
    global otms opts onrr
    buf = opts(i,j1); opts(i,j1)=opts(i,j2); opts(i,j2)=buf;
    buf = otms(i,j1); otms(i,j1)=otms(i,j2); otms(i,j2)=buf;
    buf = onrr(i,j1); onrr(i,j1)=onrr(i,j2); onrr(i,j2)=buf;
end